$('.clip_link').click(function (e) {

	var clipHREF = this.href;
	console.log("clipHREF: " + clipHREF);

  extension = clipHREF.split('.').pop();
  console.log("extension: " + extension);
  if (extension == "mp4") {

    //var source = document.createElement('source');
    //source.setAttribute('src', clipHREF);

    var video = $('<video id="videoPlayer" />', {
        width: 640,
        controls: true,
        crossorigin: true,
        type: 'video/mp4'
    });

    //$('#viewPort').empty();
    //video.appendTo($('#viewPort'));
    //$('#viewPort > source').attr('src', clipHREF);
    //$('<source />').attr('src', clipHREF).appendTo(video);
    $('#viewPort').html(video);
    //$('#videoPort video > source').attr('src', clipHREF);
    $('#videoPlayer > source').attr('src', clipHREF);
    video.load();
    video.play();

  }

  if (extension == "jpg") {
    var image = $('<img />', {
      width: 1000,
      src: clipHREF
    });

    $('#viewPort').html(image);
  }

  e.preventDefault();

});

